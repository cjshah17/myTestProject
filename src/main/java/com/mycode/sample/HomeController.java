package com.mycode.sample;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mycode.sample.model.HealthPage;
import com.mycode.sample.service.HealthPlanService;

/**
 * Created by : Chintan Shah
 * cjshah17@gmail.com
 */
@Controller
public class HomeController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	private HealthPlanService healthPlanService;

	@Autowired(required = true)
	@Qualifier(value = "healthPlanService")
	public void setHealthPlanService(HealthPlanService healthPlanService) {
		this.healthPlanService = healthPlanService;
	}

	/**
	 * Starting point. Default view
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		String formattedDate = dateFormat.format(date);
		model.addAttribute("serverTime", formattedDate);
		return "home";
	}

	// get list of data for health information
	public String landHealthPage(Model model) {
		model.addAttribute("healthpage", new HealthPage());
		model.addAttribute("listResult", this.healthPlanService.listResults());
		return "healthpageview";
	}
	
	//add Health Information
	@RequestMapping(value= "/healthpage/add", method = RequestMethod.POST)
	public String addHealthInformation(@ModelAttribute("healthpage") HealthPage p, Model model){
		if(p.getId() == 0){
			//this.healthPlanService.addHealthPlanDetails(p);
			Integer getResult = this.healthPlanService.addHealthPlanDetails(p);
			model.addAttribute("getResult" , "Health Insurance Premium for Mr. "+ p.getName()  + ": Rs." + getResult.toString());
			 model.addAttribute("healthpage",new HealthPage());
		}
		return "healthpageview";
	}
	
}
