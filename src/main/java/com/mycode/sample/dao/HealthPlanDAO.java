package com.mycode.sample.dao;

import java.util.List;

import com.mycode.sample.model.HealthPage;



public interface HealthPlanDAO {

	public void addHealthPageDetail(HealthPage p);
	public List<HealthPage> listHealthPageDetails();
}
