package com.mycode.sample.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.mycode.sample.model.HealthPage;

@Repository
public class HealthPlanDAOImpl implements HealthPlanDAO {

	private static final Logger logger = LoggerFactory.getLogger(HealthPlanDAOImpl.class);

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}


	@Override
	public void addHealthPageDetail(HealthPage p) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(p);
		logger.info("hp saved successfully, hp Details=" + p);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<HealthPage> listHealthPageDetails() {
		Session session = this.sessionFactory.getCurrentSession();
		List<HealthPage> hpList = session.createQuery("from HealthPage").list();
		for (HealthPage p : hpList) {
			logger.info(" List::" + p);
		}
		return hpList;
	}


	

}
