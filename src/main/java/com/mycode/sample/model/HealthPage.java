package com.mycode.sample.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 */
@Entity
@Table(name="HealthInformation")
public class HealthPage {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private String name;
	
	private String gender;
	
	private int age;
	
	private boolean hypertension;
	
	private boolean bp;
	
	private boolean bs;
	
	private boolean ow;
	
	private boolean smoking;
	
	private boolean alcohol;
	
	private boolean dailyexe;
	
	private boolean drugs;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public boolean isHypertension() {
		return hypertension;
	}

	public void setHypertension(boolean hypertension) {
		this.hypertension = hypertension;
	}

	public boolean isBp() {
		return bp;
	}

	public void setBp(boolean bp) {
		this.bp = bp;
	}

	public boolean isBs() {
		return bs;
	}

	public void setBs(boolean bs) {
		this.bs = bs;
	}

	public boolean isOw() {
		return ow;
	}

	public void setOw(boolean ow) {
		this.ow = ow;
	}

	public boolean isSmoking() {
		return smoking;
	}

	public void setSmoking(boolean smoking) {
		this.smoking = smoking;
	}

	public boolean isAlcohol() {
		return alcohol;
	}

	public void setAlcohol(boolean alcohol) {
		this.alcohol = alcohol;
	}

	public boolean isDailyexe() {
		return dailyexe;
	}

	public void setDailyexe(boolean dailyexe) {
		this.dailyexe = dailyexe;
	}

	public boolean isDrugs() {
		return drugs;
	}

	public void setDrugs(boolean drugs) {
		this.drugs = drugs;
	}

	@Override
	public String toString() {
		return "HealthPage [id=" + id + ", name=" + name + ", gender=" + gender + ", age=" + age + ", hypertension="
				+ hypertension + ", bp=" + bp + ", bs=" + bs + ", ow=" + ow + ", smoking=" + smoking + ", alcohol="
				+ alcohol + ", dailyexe=" + dailyexe + ", drugs=" + drugs + "]";
	}
	
	
}
