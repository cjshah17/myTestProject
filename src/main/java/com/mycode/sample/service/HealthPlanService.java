package com.mycode.sample.service;

import java.util.List;

import com.mycode.sample.model.HealthPage;


public interface HealthPlanService {

	public Integer addHealthPlanDetails(HealthPage p);
	public List<HealthPage> listResults();
	
}
