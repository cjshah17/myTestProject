package com.mycode.sample.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycode.sample.dao.HealthPlanDAO;
import com.mycode.sample.model.HealthPage;


@Service
public class HealthPlanServiceImpl implements HealthPlanService {
	
	private HealthPlanDAO healthPlanDAO;

	public void setHealthPlanDAO(HealthPlanDAO healthPlanDAO) {
		this.healthPlanDAO = healthPlanDAO;
	}

	@Override
	@Transactional
	public Integer addHealthPlanDetails(HealthPage p) {
		Integer getResult = 10;
		this.healthPlanDAO.addHealthPageDetail(p);
		getResult = getHealthPlanCalculation(p);
		return getResult;
	}

	@Override
	@Transactional
	public List<HealthPage> listResults() {
		return this.healthPlanDAO.listHealthPageDetails();
	}
	
	public Integer getHealthPlanCalculation(HealthPage hp) {
		Integer getResult = 0;
		if(hp != null && hp.getAge() >= 18)
			getResult = countAgeAndAddInBase(hp.getAge());
		else
			getResult = 5000;
		getResult = genderRuleCalculation(hp.getGender(), getResult);
		getResult = preExistingConditionCalculation(hp , getResult);
		getResult = habbitsCalculation(hp , getResult);
		
		return getResult;
	}
	private Integer habbitsCalculation(HealthPage hp, Integer getResult) {
		if(hp.isSmoking())
			getResult = getResult + (getResult * 3)/100;
		if(hp.isAlcohol())
			getResult = getResult + (getResult * 3)/100;
		if(hp.isDailyexe())
			getResult = getResult - (getResult * 3)/100;
		if(hp.isDrugs())
			getResult = getResult + (getResult * 3)/100;
		
		return getResult;
	}

	private Integer preExistingConditionCalculation(HealthPage hp, Integer getResult) {
		if(hp.isHypertension())
			getResult = getResult + (getResult * 1)/100;
		if(hp.isBp())
			getResult = getResult + (getResult * 1)/100;
		if(hp.isBs())
			getResult = getResult + (getResult * 1)/100;
		if(hp.isOw())
			getResult = getResult + (getResult * 1)/100;
		
		return getResult;
	}

	private Integer genderRuleCalculation(String gender, Integer getResult) {
		if(gender.equals("male"))
			getResult = getResult + (getResult * 2)/100;
		return getResult;
	}

	private Integer countAgeAndAddInBase(int i) {
		Integer getResult = 0; 
		if(i >= 18 && i < 40) {
			getResult = 5000 + (5000 * 10)/100;
		} else if(i > 40) {
			getResult = 5000 + (5000 * 20)/100;
		}
		
		return getResult;
	}

	

}
