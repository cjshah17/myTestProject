<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false"%>
<html>
<head>
<title>Person Page</title>
<style type="text/css">
.tg {
	border-collapse: collapse;
	border-spacing: 0;
	border-color: #ccc;
}

.tg td {
	font-family: Arial, sans-serif;
	font-size: 14px;
	padding: 10px 5px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: #ccc;
	color: #333;
	background-color: #fff;
}

.tg th {
	font-family: Arial, sans-serif;
	font-size: 14px;
	font-weight: normal;
	padding: 10px 5px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: #ccc;
	color: #333;
	background-color: #f0f0f0;
}

.tg .tg-4eph {
	background-color: #f9f9f9
}
</style>
</head>
<body>
	<h1>Add a Person Health Information</h1>

	<c:url var="addAction" value="/healthpage/add"></c:url>

	<form:form action="${addAction}" commandName="healthpage">
		<table>
		
			<tr>
				<td><form:label path="name">
						<spring:message text="Name" />
					</form:label></td>
				<td><form:input path="name" /></td>
			</tr>
			<tr>
				<td><form:label path="gender">
						<spring:message text="gender" />
					</form:label></td>
				<td>
				<select id="gender" name="gender">
						<option value="male" selected>Male</option>
						<option value="female">Female</option>
						<option value="other">Other</option>
				</select>
				</td>

			</tr>
			<tr>
				<td><form:label path="age">
						<spring:message text="age" />
					</form:label></td>
				<td><form:input path="age" /></td>
			</tr>
			<tr>
				<td><form:label path="hypertension">
						<spring:message text="hypertension" />
					</form:label></td>
				<td><select id="hypertension" name="hypertension">
						<option value="0" selected>No</option>
						<option value="1">Yes</option>
				</select></td>
			</tr>
			<tr>
				<td><form:label path="bp">
						<spring:message text="bp" />
					</form:label></td>
				<td><select id="bp" name="bp">
						<option value="0" selected>No</option>
						<option value="1">Yes</option>
				</select></td>
			</tr>
			<tr>
				<td><form:label path="bs">
						<spring:message text="bs" />
					</form:label></td>
				<td><select id="bs" name="bs">
						<option value="0" selected>No</option>
						<option value="1">Yes</option>
				</select></td>
			</tr>
			<tr>
				<td><form:label path="ow">
						<spring:message text="ow" />
					</form:label></td>
				<td><select id="ow" name="ow">
						<option value="0" selected>No</option>
						<option value="1">Yes</option>
				</select></td>
			</tr>
			<tr>
				<td><form:label path="smoking">
						<spring:message text="smoking" />
					</form:label></td>
				<td><select id="smoking" name="smoking">
						<option value="0" selected>No</option>
						<option value="1">Yes</option>
				</select></td>
			</tr>
			<tr>
				<td><form:label path="alcohol">
						<spring:message text="alcohol" />
					</form:label></td>
				<td><select id="alcohol" name="alcohol">
						<option value="0" selected>No</option>
						<option value="1">Yes</option>
				</select></td>
			</tr>
			<tr>
				<td><form:label path="dailyexe">
						<spring:message text="dailyexe" />
					</form:label></td>
				<td><select id="dailyexe" name="dailyexe">
						<option value="0" selected>No</option>
						<option value="1">Yes</option>
				</select></td>
			</tr>
			<tr>
				<td><form:label path="drugs">
						<spring:message text="drugs" />
					</form:label></td>
				<td><select id="drugs" name="drugs">
						<option value="0" selected>No</option>
						<option value="1">Yes</option>
				</select></td>
			</tr>

			<tr>
				<td colspan="2"><input type="submit"
					value="<spring:message text="Add Details"/>" /></td>
			</tr>
		</table>
	</form:form>
	<br>
	<h3>Click on Add Details to Get Health Insurance approximate estimation</h3>
	<h4 />
	${getResult}
	</h4>
</body>
</html>
