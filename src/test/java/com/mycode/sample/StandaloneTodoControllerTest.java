package com.mycode.sample;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.mycode.sample.HomeController;
import com.mycode.sample.dao.HealthPlanDAO;
import com.mycode.sample.model.HealthPage;
import com.mycode.sample.service.HealthPlanService;
/*
 * Developed by : Chintan Shah
 */
public class StandaloneTodoControllerTest {

	@InjectMocks
	private HomeController homeController;

	@Mock
	private HealthPlanDAO healthPlanDAO;

	@Mock
	private HealthPlanService healthPlanService;

	@Mock
	private HealthPage healthPage;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(homeController).build();
	}

	@Test
	public void testHealthCareStartUp() throws Exception {
		this.mockMvc.perform(get("/")).andExpect(status().isOk());
	}

	@Test
	public void testHealthCareDetails() throws Exception {

		List<HealthPage> getResult = new ArrayList<HealthPage>();
		when(healthPlanService.listResults()).thenReturn(getResult);

		this.mockMvc.perform(get("/healthpage")).andExpect(status().isOk())
				.andExpect(model().attribute("listResult", getResult)).andExpect(view().name("healthpageview"));
	}

	/*
	 * Test case on Service layer
	 */
	@Test
	public void testHealthCareCalculation() {
		/*
		 * Can write multiple testcases
		 */
		HealthPage p = new HealthPage();
		p.setName("Chintan");
		p.setGender("male");
		p.setAge(11);
		p.setHypertension(false);
		p.setBp(false);
		p.setBs(false);
		p.setOw(false);
		p.setSmoking(true);
		p.setAlcohol(true);
		p.setDailyexe(true);
		p.setDrugs(true);

		Integer expectedCalculation = healthPlanService.addHealthPlanDetails(p);

		assertEquals("5405", expectedCalculation.toString());

	}

}